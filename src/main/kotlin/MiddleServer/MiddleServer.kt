package MiddleServer

import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.nio.aAccept
import kotlinx.coroutines.experimental.nio.aConnect
import kotlinx.coroutines.experimental.nio.aRead
import kotlinx.coroutines.experimental.nio.aWrite
import kotlinx.coroutines.experimental.runBlocking
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.AsynchronousServerSocketChannel
import java.nio.channels.AsynchronousSocketChannel
import java.util.logging.Logger

val logger = Logger.getLogger("Middle Server logger")

class MiddleServer(private val addr: String, private val port: Int) {
    suspend fun handle(client: AsynchronousSocketChannel) {
        val realServer = AsynchronousSocketChannel.open()
        realServer.aConnect(InetSocketAddress(addr, port))

        async {
            relay(client, realServer)
        }

        async {
            relay(realServer, client)
        }
    }


    suspend private fun relay(fr: AsynchronousSocketChannel, to: AsynchronousSocketChannel) {
        var haveRead: Int
        val dataBuffer = ByteBuffer.allocate(4096)

        try {
            while (true) {
                haveRead = fr.aRead(dataBuffer)

                if (haveRead <= 0) {
//                    fr.shutdownInput()
//                    fr.shutdownOutput()
//                    to.shutdownInput()
//                    to.shutdownOutput()
                    break
                }

                dataBuffer.flip()

                to.aWrite(dataBuffer)
                dataBuffer.clear()
            }
        } catch (e: Throwable) {
            logger.warning(e.message)
        } finally {
            fr.close()
            to.close()
        }
    }
}

fun main(args: Array<String>) = runBlocking {
    when {
        args[0] == "-h" || args[0] == "--help" -> {
        println("(kotlin)/(java -jar) MiddleServer.jar localAddr localPort remoteAddr remotePort")
            return@runBlocking
    }
        args.size != 4 -> System.exit(1)
    }

    val serverChannel = AsynchronousServerSocketChannel.open()
    val laddr = args[0]
    val lport = args[1].toInt()
    val addr = args[2]
    val port = args[3].toInt()
    serverChannel.bind(InetSocketAddress(laddr, lport))

    val middleServer = MiddleServer(addr, port)
    while (true) {
        val client = serverChannel.aAccept()
        async { middleServer.handle(client) }
    }
}