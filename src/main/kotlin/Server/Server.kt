package Server

import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.nio.aAccept
import kotlinx.coroutines.experimental.nio.aConnect
import kotlinx.coroutines.experimental.nio.aRead
import kotlinx.coroutines.experimental.nio.aWrite
import kotlinx.coroutines.experimental.runBlocking
import java.net.Inet4Address
import java.net.Inet6Address
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.AsynchronousServerSocketChannel
import java.nio.channels.AsynchronousSocketChannel
import java.util.logging.Logger

val logger = Logger.getLogger("Server logger")

private fun makeSendPort(i: Int): ByteArray {
    val binaryNumber = StringBuffer(Integer.toBinaryString(i))
    while (binaryNumber.length <= 8) {
        binaryNumber.insert(0, '0')
    }
    val byteArray = ByteArray(2)
    byteArray[0] = Integer.parseInt(binaryNumber.substring(0 until binaryNumber.lastIndex - 7), 2).toByte()
    byteArray[1] = Integer.parseInt(binaryNumber.substring(binaryNumber.lastIndex - 7 until binaryNumber.lastIndex + 1), 2).toByte()
    return byteArray
}

private fun getPort(byteArray: ByteArray): Int {
    return (byteArray[0].toInt() and 0xFF shl 8) or (byteArray[1].toInt() and 0xFF)
}

class TCPRelay(private val client: AsynchronousSocketChannel) {
    suspend fun handle() {
        val buffer = ByteBuffer.allocate(50)

        // get version and nmethods
        try {
            client.aRead(buffer)
        } catch (e: Throwable) {
            client.close()
            return
        }
        buffer.flip()

        val versionAndNmethods = ByteArray(2)
        buffer.get(versionAndNmethods)
        buffer.compact()
        buffer.flip()

        if (versionAndNmethods[0].toInt() != 5) {
            client.close()
            logger.warning("Sock version error")
            return
        }
        logger.fine("Sock version right")

//        val nmethods = ByteArray(1)
//        buffer.get(nmethods)
//        buffer.compact()
//        buffer.flip()

        val nmethod = versionAndNmethods[1].toInt()
        val methods = ByteArray(nmethod)
        buffer.get(methods)
        buffer.compact()
        buffer.flip()

        var noAuth = false
        for (i in 0 until methods.size) {
            if (methods[i].toInt() == 0) {
                noAuth = true
                break
            }
        }

        if (!noAuth) {
            client.close()
            return
        }
        logger.fine("Use no auth")

        buffer.clear()
        buffer.put(byteArrayOf(5, 0))
        buffer.flip()
        try {
            client.aWrite(buffer)
        } catch (e: Throwable) {
            client.close()
            return
        }

        buffer.clear()
        try {
            client.aRead(buffer)
        } catch (e: Throwable) {
            client.close()
            return
        }
        buffer.flip()

        val requestHeader = ByteArray(4)
        buffer.get(requestHeader)
        buffer.compact()
        buffer.flip()

        val requestOK = when {
            requestHeader[0].toInt() !=5 -> {
                false
            }
            requestHeader[1].toInt() !=1 -> {
                false
            }
            requestHeader[2].toInt() !=0 -> {
                false
            }
            else -> {
                true
            }
        }
        if (!requestOK) {
            client.close()
            return
        }

        val atyp = requestHeader[3].toInt()
        val addr: String

        when (atyp) {
            1 -> {
                val ipv4 = ByteArray(4)
                buffer.get(ipv4)
                buffer.compact()
                buffer.flip()

                addr = Inet4Address.getByAddress(ipv4).hostAddress
            }

            3 -> {
                val addrLength = ByteArray(1)
                buffer.get(addrLength)
                buffer.compact()
                buffer.flip()

                val domainName = ByteArray(addrLength[0].toInt())
                buffer.get(domainName)
                buffer.compact()
                buffer.flip()
                addr = String(domainName)
            }

            4 -> {
                val ipv6 = ByteArray(16)
                buffer.get(ipv6)
                buffer.compact()
                buffer.flip()

                addr = Inet6Address.getByAddress(ipv6).hostAddress
            }

            else -> {
                val errAddr = Inet4Address.getByName("0.0.0.0").address
                val errPort = makeSendPort(0)
                val errResp = byteArrayOf(
                        5,
                        8,
                        0,
                        1,
                        errAddr[0],
                        errAddr[1],
                        errAddr[2],
                        errAddr[3],
                        errPort[0],
                        errPort[1]
                )

                buffer.clear()
                buffer.put(errResp)
                buffer.flip()
                try {
                    client.aWrite(buffer)
                } finally {
                    client.close()
                    return
                }
            }
        }

        val rawPort = ByteArray(2)
        buffer.get(rawPort)
        buffer.clear()
        val port = getPort(rawPort)
        logger.fine("target ip: $addr, lport: $port")

        val remote = AsynchronousSocketChannel.open()
        try {
            remote.aConnect(InetSocketAddress(addr, port))
        } catch (e: Throwable) {
            client.close()
            remote.close()
            return
        }

        val bindAddr = (remote.localAddress as InetSocketAddress).address.address
        val bindPort = makeSendPort((remote.localAddress as InetSocketAddress).port)
        val resp = byteArrayOf(5, 0, 0, 1, bindAddr[0], bindAddr[1], bindAddr[2], bindAddr[3], bindPort[0], bindPort[1])
        buffer.put(resp)
        buffer.flip()
        try {
            client.aWrite(buffer)
        } catch (e: Throwable) {
            client.close()
            remote.close()
            return
        }

        async {
            Relay(client, remote)
        }

        async {
            Relay(remote, client)
        }
    }

    private suspend fun Relay(from: AsynchronousSocketChannel, to: AsynchronousSocketChannel) {
        val buffer = ByteBuffer.allocate(4096)
        while (true) {
            val haveRead: Int
            try {
                haveRead = from.aRead(buffer)
//            println("Read $haveRead bytes")
            } catch (e: Throwable) {
                from.close()
                to.close()
                return
            }

            if (haveRead <= 0) {
                from.close()
                to.close()
                return
            }
            buffer.flip()

            try {
                to.aWrite(buffer)
//            println("Write $havaWrite bytes")
            } catch (e: Throwable) {
                from.close()
                to.close()
                return
            }
            buffer.clear()
        }
    }
}

fun main(args: Array<String>) = runBlocking {
    val serverChannel = AsynchronousServerSocketChannel.open()
    serverChannel.bind(InetSocketAddress("127.0.0.2", 1089))
    while (true) {
        val client = serverChannel.aAccept()
        async {
            val tcpRelay = TCPRelay(client)
            tcpRelay.handle()
        }
    }
}