package ThreadServer

import java.io.IOException
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.SocketException
import java.nio.ByteBuffer
import java.nio.channels.SelectionKey
import java.nio.channels.Selector
import java.nio.channels.ServerSocketChannel
import java.nio.channels.SocketChannel
import java.util.logging.Logger

val logger = Logger.getLogger("KSocks Server")

class Handler(sock: SocketChannel) : Thread() {
    private val sock = sock
    private val buffer = ByteBuffer.allocate(4096)
    private val selector = Selector.open()

    override fun run() {
        val sockVersion = ByteArray(1)
        try {
            sock.read(buffer)
        } catch (e: IOException) {
            sock.close()
        } catch (e: SocketException) {
            sock.close()
        }
        buffer.flip()

        buffer.get(sockVersion)
        buffer.compact()
        buffer.flip()

        if (sockVersion[0].toInt() != 0x05) {
//            println("version error")
            logger.warning("version error")
            sock.close()
            return
        }
//        println("version right")
        logger.fine("version right")

        val nmethods = ByteArray(1)
        buffer.get(nmethods)
        buffer.compact()
        buffer.flip()

        val nmethod = nmethods[0].toInt()
        val methods = ByteArray(nmethod)
        buffer.get(methods)
        var noAuth = false
        for (i in 0 until methods.size) {
            if (methods[i].toInt() == 0) {
                noAuth = true
                break
            }
        }
        if (!noAuth) {
            sock.close()
            return
        }

        buffer.clear()
        buffer.put(byteArrayOf(0x05, 0x00))
        buffer.flip()
        try {
            sock.write(buffer)
        } catch (e: IOException) {
            sock.close()
            return
        } catch (e: SocketException) {
            sock.close()
            return
        }

        buffer.clear()
//        println("use no-auth")
        logger.fine("use no-auth")
        try {
            sock.read(buffer)
        } catch (e: IOException) {
            sock.close()
            return
        } catch (e: SocketException) {
            sock.close()
            return
        }
        buffer.flip()

        val request = ByteArray(4)
        buffer.get(request)
        buffer.compact()
        buffer.flip()

        if (request[0].toInt() != 5) {
            sock.close()
            return
        }
        if (request[1].toInt() != 1) {
            sock.close()
            return
        }
        if (request[2].toInt() != 0) {
            sock.close()
            return
        }

        val atyp = request[3].toInt()
        val addr: String

//        println("atyp: $atyp")
        logger.fine("atyp: $atyp")

        when (atyp) {
            1 -> {
                val ipv4 = ByteArray(4)
                buffer.get(ipv4)
                buffer.compact()
                buffer.flip()
                addr = InetAddress.getByAddress(ipv4).hostAddress
            }

            3 -> {
                val dataLength = ByteArray(1)
                buffer.get(dataLength)
                buffer.compact()
                buffer.flip()
                val domainName = ByteArray(dataLength[0].toInt())
                buffer.get(domainName)
                buffer.compact()
                buffer.flip()
                addr = String(domainName)
            }

            4 -> {
                val ipv6 = ByteArray(16)
                buffer.get(ipv6)
                buffer.compact()
                buffer.flip()
                addr = InetAddress.getByAddress(ipv6).hostAddress
            }

            else -> {
                val errAddr = InetAddress.getByName("0.0.0.0").address
                val errPort = makeSendPort(0)
                val errResp = byteArrayOf(5, 8, 0, 1, errAddr[0], errAddr[1], errAddr[2], errAddr[3], errPort[0], errPort[1])
                buffer.clear()
                buffer.put(errResp)
                buffer.flip()
                try {
                    sock.write(buffer)
                } finally {
                    sock.close()
                }
                return
            }
        }
        val rawPort = ByteArray(2)
        buffer.get(rawPort)
        buffer.clear()
        val port = getPort(rawPort)
//        println("target ip: $laddr, lport: $lport")
        logger.fine("target ip: $addr, lport: $port")

        val remote = SocketChannel.open()
        try {
            remote.connect(InetSocketAddress(addr, port))
        } catch (e: IOException) {
            sock.close()
            remote.close()
            return
        } catch (e: SocketException) {
            sock.close()
            remote.close()
            return
        }

        val bindAddr = remote.socket().localAddress.address
        val bindPort = makeSendPort(remote.socket().localPort)
        val resp = byteArrayOf(5, 0, 0, 1, bindAddr[0], bindAddr[1], bindAddr[2], bindAddr[3], bindPort[0], bindPort[1])
        buffer.put(resp)
        buffer.flip()
        try {
            sock.write(buffer)
        } catch (e: IOException) {
            sock.close()
            remote.close()
            return
        } catch (e: SocketException) {
            sock.close()
            remote.close()
            return
        }

        sock.configureBlocking(false)
        remote.configureBlocking(false)

        sock.register(selector, SelectionKey.OP_READ, arrayOf(sock, remote))
        remote.register(selector, SelectionKey.OP_READ, arrayOf(remote, sock))

        selector@while (true) {
            selector.select() > 0
            logger.finer("Start relay")
            val keys = selector.selectedKeys()
            val iterator = keys.iterator()

            while (iterator.hasNext()) {
                val key = iterator.next()
                val array = key.attachment() as Array<SocketChannel>
                val sock = array!![0]
                val remote = array[1]
                if (relay(sock, remote) == "Close") {
                    key.cancel()
                    break@selector
                }
                iterator.remove()
            }
        }
        selector.close()
        logger.fine("A relay close")
    }

    private fun relay(sock: SocketChannel, remote: SocketChannel): String? {
        buffer.clear()
        try {
            val haveRead = sock.read(buffer)
            if (haveRead <= 0) {
                sock.close()
                remote.close()
                return "Close"
            }
        } catch (e: IOException) {
            sock.close()
            remote.close()
            return "Close"
        } catch (e: SocketException) {
            sock.close()
            remote.close()
            return "Close"
        }

        buffer.flip()
        try {
            sendAll(remote)
        } catch (e: IOException) {
            sock.close()
            remote.close()
            return "Close"
        } catch (e: SocketException) {
            sock.close()
            remote.close()
            return "Close"
        }

        return null
    }

    private fun sendAll(remote: SocketChannel) {
        while (buffer.hasRemaining()) {
            remote.write(buffer)
            buffer.compact()
            buffer.flip()
        }
    }
}

private fun makeSendPort(i: Int): ByteArray {
    val binaryNumber = StringBuffer(Integer.toBinaryString(i))
    while (binaryNumber.length <= 8) {
        binaryNumber.insert(0, '0')
    }
    val byteArray = ByteArray(2)
    byteArray[0] = Integer.parseInt(binaryNumber.substring(0 until binaryNumber.lastIndex - 7), 2).toByte()
    byteArray[1] = Integer.parseInt(binaryNumber.substring(binaryNumber.lastIndex - 7 until binaryNumber.lastIndex + 1), 2).toByte()
    return byteArray
}

private fun getPort(byteArray: ByteArray): Int {
    return (byteArray[0].toInt() and 0xFF shl 8) or (byteArray[1].toInt() and 0xFF)
}

fun main(args: Array<String>) {
    val server = ServerSocketChannel.open()
    server.bind(InetSocketAddress("127.0.0.2", 1089))
    while (true) {
        val sock = server.accept()
        val handle = Handler(sock)
        handle.start()
    }
}


